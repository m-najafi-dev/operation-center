package ir.operation.server.person.dto.request;

import ir.operation.server.person.dto.base.PersonInfoBaseDTO;
import ir.operation.server.utilities.protocol.ProtocolRequest;
import lombok.Data;

@Data
public class PersonInfoRequestDTO extends PersonInfoBaseDTO implements ProtocolRequest {

    private String phoneHome;
    private String mobile;
    private String address;
    private long lockVersion;
}
