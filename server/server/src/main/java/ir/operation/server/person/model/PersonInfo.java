package ir.operation.server.person.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "PERSON_INFO")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class PersonInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(length = 11)
    private String phoneHome;

    @Column(nullable = false, length = 11)
    private String mobile;

    @Column(unique = true, nullable = false, length = 75)
    private String email;

    @Column(unique = true, nullable = false, length = 10)
    private String nationalCode;

    @Column(length = 250)
    private String address;

    @Column(length = 10)
    private String postCode;

    @CreationTimestamp
    private LocalDateTime creationTimestamp;

    @Version
    private long lockVersion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PersonInfo that = (PersonInfo) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
