package ir.operation.server.person.dto.base;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class PersonInfoBaseDTO {
    @NotBlank(message = "Not Null FirstName")
    private String firstName;
    @NotBlank(message = "Not Null LastName")
    private String lastName;
    @NotBlank(message = "Not Null NationalCode")
    private String nationalCode;
    @NotBlank(message = "Not Null PostCode")
    private String postCode;
    @Email(message = "InCorrect Pattern Email")
    private String email;
}
