package ir.operation.server.person.dto.response;

import ir.operation.server.person.dto.base.PersonInfoBaseDTO;
import ir.operation.server.utilities.protocol.ProtocolResponse;

import java.time.LocalDateTime;

public class PersonInfoResponseDTO extends PersonInfoBaseDTO implements ProtocolResponse {
    private Long id;

    private String phoneHome;

    private String mobile;

    private String address;

    private LocalDateTime creationTimestamp;

    private long lockVersion;
}
