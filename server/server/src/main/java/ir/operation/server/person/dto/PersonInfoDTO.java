package ir.operation.server.person.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.operation.server.utilities.protocol.ProtocolResponse;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonInfoDTO implements ProtocolResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String nationalCode;
}
